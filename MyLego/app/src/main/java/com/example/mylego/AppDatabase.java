package com.example.mylego;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities={Stock_parts.class,Colors.class,Colors_code_id.class,Inventories.class,Inventory_parts.class,Missing_parts.class,Parts.class,Part_categories.class,Sets.class,Set_collection.class,Themes.class},version=1,exportSchema=false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract Stock_partsDao stock_partsDao();
    public abstract ColorsDao colorsDao();
    public abstract Colors_code_idDao colors_code_idDao();
    public abstract InventoriesDao inventoriesDao();
    public abstract Inventory_partsDao inventory_partsDao();
    public abstract Missing_partsDao missing_partsDao();
    public abstract PartsDao partsDao();
    public abstract Part_categoriesDao part_categoriesDao();
    public abstract SetsDao setsDao();
    public abstract Set_collectionDao set_collectionDao();
    public abstract ThemesDao themesDao();

}
