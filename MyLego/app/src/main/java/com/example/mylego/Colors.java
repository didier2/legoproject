package com.example.mylego;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName="colors", indices = { @Index(value={"id","name","rgb","is_trans"})})
public class Colors {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name="id")
    private int id;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @NonNull
    @ColumnInfo(name = "rgb")
    private String rgb;

    @NonNull
    @ColumnInfo(name = "is_trans")
    private int isTrans;

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id=id;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name=name;
    }

    public String getRgb(){
        return this.rgb;
    }

    public void setRgb(String r){
        this.rgb=r;
    }

    public int getIsTrans(){
        return this.isTrans;
    }

    public void setIsTrans(int it){
        this.isTrans=it;
    }
}
