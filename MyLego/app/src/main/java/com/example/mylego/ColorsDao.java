package com.example.mylego;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ColorsDao {

    @Query("SELECT COUNT(*) FROM colors")
    int count();

    @Query("SELECT * FROM colors WHERE id = :id")
    List<Colors> getById(int id);

    @Insert
    long[] insertAll(Colors... colors);
}
