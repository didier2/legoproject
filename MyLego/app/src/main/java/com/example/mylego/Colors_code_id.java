package com.example.mylego;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName="colors_code_id", indices = { @Index(value={"colors_id","bricklink_id"})})
public class Colors_code_id {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name="colors_id")
    private int colorsId;

    @NonNull
    @ColumnInfo(name = "bricklink_id")
    private String bricklinkId;

    public int getColorsId(){
        return this.colorsId;
    }

    public void setColorsId(int c){
        this.colorsId=c;
    }

    public String getBricklinkId(){
        return this.bricklinkId;
    }

    public void setBricklinkId(String bi){
        this.bricklinkId=bi;
    }
}
