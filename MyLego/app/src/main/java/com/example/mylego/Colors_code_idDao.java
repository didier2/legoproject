package com.example.mylego;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Colors_code_idDao {
    @Query("SELECT COUNT(*) FROM colors_code_id")
    int count();

    @Insert
    long[] insertAll(Colors_code_id... colors_code_ids);
}
