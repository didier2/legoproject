package com.example.mylego;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CriteresSetsActivity extends AppCompatActivity {

    private DatabaseClient mDb;
    AutoCompleteTextView actv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criteres_sets);
        mDb = DatabaseClient.getInstance(getApplicationContext());

        List<String> list = remplirListe();
        actv=findViewById(R.id.actv);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,list);
        actv.setAdapter(adapter);


    }

    public void valider(View view) {
        EditText vueMin = findViewById(R.id.min);
        EditText vueMax = findViewById(R.id.max);
        CheckBox vueCouleur = findViewById(R.id.color);
        int min;
        int max;
        if(vueMin.getText().toString().equals("")){
            min=0;
        }else if (Integer.parseInt(vueMin.getText().toString())>100) {
            vueMin.setError("100% maximum");
            vueMin.requestFocus();
            return;
        }else{
            min=Integer.parseInt(vueMin.getText().toString());
        }

        if(vueMax.getText().toString().equals("")){
            max=100;
        }else if (Integer.parseInt(vueMax.getText().toString())>100) {
            vueMax.setError("100% maximum");
            vueMax.requestFocus();
            return;
        }else{
            max=Integer.parseInt(vueMax.getText().toString());
        }

        if (min>max){
            vueMin.setError("Le minimum doit être inferieur au maximum");
            vueMin.requestFocus();
            return;
        }


        Intent intent=new Intent(this, ListeSetsActivity.class);
        intent.putExtra(ListeSetsActivity.MIN,min);
        intent.putExtra(ListeSetsActivity.MAX,max);
        intent.putExtra(ListeSetsActivity.COULEUR, vueCouleur.isChecked());
        intent.putExtra(ListeSetsActivity.THEME,actv.getText().toString());
        intent.putExtra(ListeSetsActivity.MESSETS,false);
        startActivity(intent);
        finish();
    }

//    public void annuler(View view){
//        Intent intent=new Intent(this,MainActivity.class);
//        startActivity(intent);
//        finish();
//    }


    public List<String> remplirListe(){
        class RemplirListe extends AsyncTask<Void, Void, List<Themes>> {

            List<String> liste=new ArrayList<>();

            public List<String> getListe(){
                return liste;
            }

            @Override
            protected List<Themes> doInBackground(Void... voids) {
               // List<Themes> themes =(mDb.getAppDatabase().themesDao().getPrincipaux());
                List<Themes> themes=new ArrayList<>();
                return themes;
            }

            @Override
            protected void onPostExecute(List<Themes> themes) {
                super.onPostExecute(themes);

                liste.add("indifférent");
                liste.add("cate1");
                liste.add("cate2");
                for(Themes theme:themes){
                    //     liste.add(theme.getName());
                }
            }
        }

        //////////////////////////
        // IMPORTANT bien penser à executer la demande asynchrone
        // Création d'un objet de type GetTasks et execution de la demande asynchrone
        RemplirListe rl = new RemplirListe();
        rl.execute();
        return rl.getListe();
    }

}
