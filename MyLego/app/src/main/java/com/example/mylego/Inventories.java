package com.example.mylego;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName="inventories", indices = { @Index(value={"id","version","set_num"})})
public class Inventories {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="id")
    private int id;

    @NonNull
    @ColumnInfo(name = "version", defaultValue = "1")
    private int version;

    @NonNull
    @ColumnInfo(name = "set_num")
    private String setNum;

    public int getId(){
        return this.id;
    }

    public void setId(int i){
        this.id=i;
    }
    public int getVersion(){
        return this.version;
    }

    public void setVersion(int v){
        this.version=v;
    }
    public String getSetNum(){
        return this.setNum;
    }

    public void setSetNum(String sn){
        this.setNum=sn;
    }


}
