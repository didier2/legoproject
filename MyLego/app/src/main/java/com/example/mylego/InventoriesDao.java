package com.example.mylego;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface InventoriesDao {

    @Query("SELECT COUNT(*) FROM inventories")
    int count();

    @Query("SELECT * FROM inventories WHERE id=:id")
    List<Inventories> getById(int id);

//    @Query("SELECT * " +
//            "FROM inventories " +
//            "WHERE (set_num = (SELECT set_num " +
//                               "FROM sets " +
//                                "WHERE theme_id = (SELECT id " +
//                                                  "FROM themes " +
//                                                    "WHERE name= :theme OR parent_id = (SELECT id " +
//                                                                                        "FROM themes " +
//                                                                                        "WHERE name= :theme OR parent_id = (SELECT id " +
//                                                                                                                            "FROM themes " +
//                                                                                                                            "WHERE name= :theme)))))"+
//            "EXCEPT SELECT * FROM inventories WHERE id=(SELECT id FROM set_collection)")
//    List<Inventories> getInventoriesHorsCollection(String theme);

    @Query("SELECT i.id, i.version, i.set_num " +
            "FROM inventories i, sets s, themes t " +
            "WHERE i.set_num = s.set_num AND s.theme_id = t.id AND (t.name=:theme OR t.parent_id=(SELECT id FROM themes WHERE name=:theme OR parent_id=(SELECT id FROM themes WHERE name=:theme))) " +
            "EXCEPT SELECT * FROM inventories WHERE id=(SELECT inventory_id FROM set_collection)")
    List<Inventories> getInventoriesHorsCollectionByTheme(String theme);

    @Query("SELECT * FROM inventories ")//EXCEPT (SELECT * FROM inventories WHERE id=(SELECT inventory_id FROM set_collection))")
    List<Inventories> getInventoriesHorsCollection();

    @Insert
    long[] insertAll(Inventories... inventories);
}
