package com.example.mylego;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName="inventory_parts", indices = { @Index(value={"inventory_parts_id","inventory_id","part_num","color_id","quantity","is_spare"})})
public class Inventory_parts {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="inventory_parts_id")
    private int inventoryPartsId;

    @NonNull
    @ColumnInfo(name="inventory_id")
    private int inventoryId;

    @NonNull
    @ColumnInfo(name="part_num")
    private String partNum;

    @NonNull
    @ColumnInfo(name="color_id")
    private int colorId;

    @NonNull
    @ColumnInfo(name="quantity")
    private int quantity;

    @NonNull
    @ColumnInfo(name="is_spare")
    private boolean isSpare;

    public int getInventoryPartsId() {
        return inventoryPartsId;
    }

    public void setInventoryPartsId(int inventoryPartsId) {
        this.inventoryPartsId = inventoryPartsId;
    }

    public void setInventoryId(int inventoryId) {
        this.inventoryId = inventoryId;
    }

    public void setPartNum(String partNum) {
        this.partNum = partNum;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setSpare(boolean spare) {
        isSpare = spare;
    }



    public int getInventoryId() {
        return inventoryId;
    }

    public String getPartNum() {
        return partNum;
    }

    public int getColorId() {
        return colorId;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean isSpare() {
        return isSpare;
    }






}
