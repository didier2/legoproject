package com.example.mylego;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Inventory_partsDao {
    @Query("SELECT COUNT(*) FROM inventory_parts")
    int count();

    @Query("SELECT * FROM inventory_parts WHERE inventory_id=:id")
    List<Inventory_parts> getByInventoryId(int id);

    @Insert
    long[] insertAll(Inventory_parts... inventory_parts);
}
