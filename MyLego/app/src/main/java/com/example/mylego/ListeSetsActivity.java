package com.example.mylego;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListeSetsActivity extends AppCompatActivity {

    private static final String TAG = "ListeSetsActivity";

    public static final String MIN="min";
    public static final String MAX="max";
    public static final String COULEUR="couleur";
    public static final String THEME="theme";
    public static final String MESSETS="messets";
    private DatabaseClient mDb;
    private ArrayList<String> images=new ArrayList<>();
    private ArrayList<String> names=new ArrayList<>();
    private String ctxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        mDb = DatabaseClient.getInstance(getApplicationContext());
        if(getIntent().getBooleanExtra(MESSETS,false)==true){
            ctxt="mesSets";
            //initialiserListesMesSets();
        }else{
            ctxt="autresSets";
            //initialiserListes();
        }
        images.add("https://img.bricklink.com/ItemImage/SN/0/1.png");
        images.add("https://img.bricklink.com/ItemImage/SN/0/1.png");
        names.add("set1\nset_num = 1, version = 2\ntaux = 88%");
        names.add("set2");
        initRecyclerView();
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        finish();
//    }


    private void initRecyclerView(){
        RecyclerView recyclerView=findViewById(R.id.recycler_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this,names,images,ctxt);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }



    private void initialiserListes(){
        class InitialiserListes extends AsyncTask<Void, Void, List<Inventories>> {

            @Override
            protected List<Inventories> doInBackground(Void... voids) {
                List<Inventories> inventoriesList;
                if(!getIntent().getStringExtra(THEME).equals("")){
                    inventoriesList = mDb.getAppDatabase().inventoriesDao().getInventoriesHorsCollectionByTheme(getIntent().getStringExtra(THEME));
                }else{
                    inventoriesList = mDb.getAppDatabase().inventoriesDao().getInventoriesHorsCollection();
                }
                //checker le taux

                for (Inventories inventaire:inventoriesList) {

                    //pour chaque inventaire recuperer la liste des pieces necessaires en base de donnée
                    List<Inventory_parts> piecesNecessaires = mDb.getAppDatabase().inventory_partsDao().getByInventoryId(inventaire.getId());
                    //variables du nombre de piece necessaires et du nombre de pieces possedées pour calculer un taux de possession
                    int nbPiecesNecessaires = 0;
                    int nbPiecesEnStock = 0;

                    //pour chaque piece de la recette (piece necessaire)...
                    for (Inventory_parts ip:piecesNecessaires){
                        int quantite=ip.getQuantity();
                        //...on ajoute sa quantité aux pieces necessaires...
                        nbPiecesNecessaires=nbPiecesNecessaires+quantite;

                        //...puis on recupere les pieces en stock correspondantes pour les ajouter a nbpiecesStock
                        List<Stock_parts> mesPieces;
                        if(getIntent().getBooleanExtra(COULEUR,false)){
                            //recupere les objet de la piece correspondante en stock (meme piece meme couleur)
                            mesPieces = mDb.getAppDatabase().stock_partsDao().getByPartNumAndColorId(ip.getPartNum(),ip.getColorId());

                        }else {
                            //recupere les pieces correspondantes sans prendre en compte la couleur
                            mesPieces = mDb.getAppDatabase().stock_partsDao().getByPartNum(ip.getPartNum());
                        }
                        //total=nombre total de pieces correspondante a la piece necessaire
                        int total=0;
                        for (Stock_parts piece:mesPieces){
                            if(piece.getQuantity()>0){
                                total=total+piece.getQuantity();
                            }

                        }
                        //ajout des pieces en stock a nbStock
                        if(total >= quantite){
                            nbPiecesEnStock=nbPiecesEnStock+quantite;
                        }else{
                            nbPiecesEnStock=nbPiecesEnStock+total;
                        }


                    }
                    float taux=(float)nbPiecesEnStock/nbPiecesNecessaires;
                    if (taux*100 >= getIntent().getIntExtra(MIN,0) && taux*100 <= getIntent().getIntExtra(MAX,0)){
                        //on recupere le set pour afficher son nom
                        List<Sets> testSets = mDb.getAppDatabase().setsDao().getSet(inventaire.getSetNum());
                        if(testSets.size()>1){
                            Toast.makeText(getApplicationContext(), "plusieurs sets avec le meme set_num", Toast.LENGTH_SHORT).show();
                        }
                        if (testSets.size()!=0){
                            Sets set = testSets.get(0);

                            images.add("https://img.bricklink.com/ItemImage/SN/0/" + set.getSetNum() + ".png");
                            names.add(set.getName()+"\nset_num = "+set.getSetNum()+", version = "+Integer.toString(inventaire.getVersion())+"\ntaux = "+taux*100+"%");
                        }else{
                            Log.d(TAG, "doInBackground: (taux) testSets.size()==0");
                        }

                    }
                }


                return inventoriesList;
            }

            @Override
            protected void onPostExecute(List<Inventories> inventoriesList) {
                super.onPostExecute(inventoriesList);




            }
        }

        //////////////////////////
        // IMPORTANT bien penser à executer la demande asynchrone
        // Création d'un objet de type GetTasks et execution de la demande asynchrone
        InitialiserListes i = new InitialiserListes();
        i.execute();
    }

    private void initialiserListesMesSets(){
        class InitialiserListesMesSets extends AsyncTask<Void, Void, List<Set_collection>> {

            @Override
            protected List<Set_collection> doInBackground(Void... voids) {
                List<Set_collection> mesInventaires = mDb.getAppDatabase().set_collectionDao().getAll();
                for(Set_collection monSet:mesInventaires){
                    //on recupere l'inventaire
                    List<Inventories> testInventories = mDb.getAppDatabase().inventoriesDao().getById(monSet.getInventory_id());
                    if(testInventories.size()>1){
                        Toast.makeText(getApplicationContext(), "plusieurs inventories avec le meme id", Toast.LENGTH_SHORT).show();
                    }
                    if(testInventories.size()!=0){
                        Inventories inventory = testInventories.get(0);
                        //on recupere le set
                        List<Sets> testSets = mDb.getAppDatabase().setsDao().getSet(inventory.getSetNum());
                        if(testSets.size()>1){
                            Toast.makeText(getApplicationContext(), "plusieurs sets avec le meme set_num", Toast.LENGTH_SHORT).show();
                        }
                        if(testSets.size()!=0){
                            Sets set = testSets.get(0);
                            //on rempli la liste les valeurs
                            names.add(set.getName()+"\nset_num = "+set.getSetNum()+", version = "+inventory.getVersion());
                        }else{
                            Log.d(TAG, "doInBackground: Mes sets : testSets.size()==0");
                        }

                    }else{
                        Log.d(TAG, "doInBackground: Mes sets : testInventories.size()==0");
                    }

                }

                return mesInventaires;
            }

            @Override
            protected void onPostExecute(List<Set_collection> mesInventaires) {
                super.onPostExecute(mesInventaires);





            }
        }

        //////////////////////////
        // IMPORTANT bien penser à executer la demande asynchrone
        // Création d'un objet de type GetTasks et execution de la demande asynchrone
        InitialiserListesMesSets i = new InitialiserListesMesSets();
        i.execute();
    }
//

}
