package com.example.mylego;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button nbSets;
    private DatabaseClient mDb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nbSets=findViewById(R.id.test);
        mDb = DatabaseClient.getInstance(getApplicationContext());



    }

    public void compteSets(View view){

            ///////////////////////
            // Classe asynchrone permettant de récupérer des taches et de mettre à jour le listView de l'activité
            class GetNbSets extends AsyncTask<Void, Void, Sets> {


                @Override
                protected Sets doInBackground(Void... voids) {
                    int nbSet = mDb.getAppDatabase().setsDao().count();

                    nbSets.setText(Integer.toString(nbSet));
                    Sets set=new Sets();
                    return set;

                }

                @Override
                protected void onPostExecute(Sets set) {
                    super.onPostExecute(set);




                }
            }

            //////////////////////////
            // IMPORTANT bien penser à executer la demande asynchrone
            // Création d'un objet de type GetTasks et execution de la demande asynchrone
            GetNbSets gns = new GetNbSets();
            gns.execute();
    }


    public void ajouterSet(View view){
        Intent intent=new Intent(this,CriteresSetsActivity.class);
        startActivity(intent);
    }

    public void collectionSets (View view){
        Intent intent=new Intent(this,ListeSetsActivity.class);
        intent.putExtra(ListeSetsActivity.MESSETS,true);
        startActivity(intent);
    }

    public void piecesInutiles(View view){
        Intent intent=new Intent(this, SelectionMaxSetsActivity.class);
        startActivity(intent);
    }
}
