package com.example.mylego;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Missing_partsDao {

    @Query("SELECT COUNT(*) FROM missing_parts")
    int count();

    @Query("SELECT * FROM missing_parts WHERE inventory_id = (SELECT id FROM inventories WHERE set_num = :setNum AND version = :version)")
    List<Missing_parts> getBySetNumAndVersion(String setNum, int version);

    @Insert
    long[] insertAll(Missing_parts... missing_parts);
}
