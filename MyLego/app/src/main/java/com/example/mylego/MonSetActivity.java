package com.example.mylego;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MonSetActivity extends AppCompatActivity {

    public static final String NAME="name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mon_set);
        TextView tv = findViewById(R.id.infoSet);
        tv.setText(getIntent().getStringExtra(NAME));
    }

    public void piecesManquantes(View view){
        Intent intent =new Intent(this,ListePartsActivity.class);
        intent.putExtra(ListePartsActivity.SETNAME,getIntent().getStringExtra(NAME));
        intent.putExtra(ListePartsActivity.PIECESMANQUANTES,true);
        startActivity(intent);
    }
}
