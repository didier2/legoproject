package com.example.mylego;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Part_categoriesDao {
    @Query("SELECT COUNT(*) FROM part_categories")
    int count();

    @Insert
    long[] insertAll(Part_categories... part_categories);
}
