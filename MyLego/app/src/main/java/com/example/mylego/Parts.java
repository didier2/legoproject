package com.example.mylego;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName="parts", indices = { @Index(value={"parts_id","part_num","name","part_cat_id"})})
public class Parts {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="parts_id")
    private int partsId;

    @NonNull
    @ColumnInfo(name="part_num")
    private String partNum;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @Nullable
    @ColumnInfo(name="part_cat_id", defaultValue = "NULL")
    private String partCatId;

    public int getPartsId() {
        return partsId;
    }

    public void setPartsId(int partsId) {
        this.partsId = partsId;
    }

    public String getPartNum() {
        return partNum;
    }

    public void setPartNum(String partNum) {
        this.partNum = partNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPartCatId() {
        return partCatId;
    }

    public void setPartCatId(String partCatId) {
        this.partCatId = partCatId;
    }
}
