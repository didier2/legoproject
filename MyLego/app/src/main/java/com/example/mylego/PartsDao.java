package com.example.mylego;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PartsDao {

    @Query("SELECT COUNT(*) FROM parts")
    int count();

    @Query("SELECT * FROM parts WHERE part_num = :partNum")
    List<Parts> getByPartNum(String partNum);

    @Query("SELECT * FROM parts WHERE part_num=(SELECT part_num FROM stock_parts WHERE part_num=(SELECT part_num FROM inventory_parts GROUP BY part_num HAVING COUNT(distinct inventory_id) <= :max))")
    List<Parts> getInStockPartByMaxSets(int max);

    @Insert
    long[] insertAll(Parts... parts);
}
