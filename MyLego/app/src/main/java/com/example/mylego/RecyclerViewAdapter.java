package com.example.mylego;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "RecyclerViewAdapter";

    private ArrayList<String> names=new ArrayList<>();
    private ArrayList<String> images=new ArrayList<>();
    private Context context;
    private String ctxt;

    public RecyclerViewAdapter(Context context, ArrayList<String> names, ArrayList<String> images,String ctxt) {
        this.names = names;
        this.images = images;
        this.context = context;
        this.ctxt=ctxt;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called.");
        if(images.get(position)!="pasdimage"){
            Glide.with(context)
                .asBitmap()
                .load(images.get(position))
                .into(holder.image);
        }

        holder.name.setText(names.get(position));
        final int itemPosition=position;

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ctxt=="mesSets"){
                    Intent intent=new Intent(v.getContext(),MonSetActivity.class);
                    intent.putExtra(MonSetActivity.NAME,names.get(itemPosition));
                    v.getContext().startActivity(intent);
                }else if(ctxt=="autresSets"){

                }
                Log.d(TAG, "onClick: "+holder.name.getText()+"ctxt ="+ctxt);
            }
        });
    }

    @Override
    public int getItemCount() {
        return names.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        TextView name;
        RelativeLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.image);
            name=itemView.findViewById(R.id.name);
            parentLayout=itemView.findViewById(R.id.parent_layout);
        }
    }
}
