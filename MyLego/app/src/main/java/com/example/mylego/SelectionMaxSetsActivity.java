package com.example.mylego;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SelectionMaxSetsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_max_sets);

    }

    public void rechercher(View view) {
        EditText vueMax=findViewById(R.id.etn);

        if (vueMax.getText().toString().equals("")) {
            vueMax.setError("à remplir");
            vueMax.requestFocus();
            return;
        }else{
            Intent intent=new Intent(this,ListePartsActivity.class);
            intent.putExtra(ListePartsActivity.MAX,Integer.parseInt(vueMax.getText().toString()));
            startActivity(intent);
            finish();
        }
    }
}
