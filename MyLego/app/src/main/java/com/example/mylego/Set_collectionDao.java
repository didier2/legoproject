package com.example.mylego;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Set_collectionDao {

    @Query("SELECT COUNT(*) FROM set_collection")
    int count();

    @Query("SELECT * FROM set_collection")
    List<Set_collection> getAll();

    @Insert
    long[] insertAll(Set_collection... set_collections);
}
