package com.example.mylego;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName="sets", indices = { @Index(value={"sets_id","set_num","name","year","theme_id","num_parts"})})
public class Sets {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="sets_id")
    private int setsId;

    @NonNull
    @ColumnInfo(name="set_num")
    private String setNum;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="year")
    private int year;

    @NonNull
    @ColumnInfo(name="theme_id")
    private int themeId;

    @NonNull
    @ColumnInfo(name="num_parts")
    private int numParts;

    public int getSetsId() {
        return setsId;
    }

    public void setSetsId(int setsId) {
        this.setsId = setsId;
    }

    public String getSetNum() {
        return setNum;
    }

    public void setSetNum(String setNum) {
        this.setNum = setNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getThemeId() {
        return themeId;
    }

    public void setThemeId(int themeId) {
        this.themeId = themeId;
    }

    public int getNumParts() {
        return numParts;
    }

    public void setNumParts(int numParts) {
        this.numParts = numParts;
    }
}
