package com.example.mylego;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SetsDao {

    @Insert
    long insert(Sets sets);

    @Query("SELECT COUNT(*) FROM sets")
    int count();

    @Insert
    long[] insertAll(Sets... sets);

    @Query("DELETE FROM sets")
    void deleteAll();

    @Query("SELECT * FROM sets where set_num= :setNum")
    List<Sets> getSet(String setNum);
}
