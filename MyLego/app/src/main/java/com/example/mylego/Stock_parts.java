package com.example.mylego;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName="stock_parts", indices = { @Index(value={"id","part_num","color_id","quantity"})})
public class Stock_parts {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="id")
    private int id;

    @NonNull
    @ColumnInfo(name = "part_num")
    private String partNum;

    @Nullable
    @ColumnInfo(name = "color_id")//, defaultValue = "NULL")
    private int colorId;

    @NonNull
    @ColumnInfo(name = "quantity")
    private int quantity;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPartNum() {
        return this.partNum;
    }

    public void setPartNum(String pn){
        this.partNum=pn;
    }


    public @Nullable int getColorId(){
        return this.colorId;
    }

    public void setColorId(@Nullable int ci){
        this.colorId=ci;
    }

    public int getQuantity(){
        return this.quantity;
    }

    public void setQuantity(int q){
        this.quantity=q;
    }
}