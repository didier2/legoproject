package com.example.mylego;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Stock_partsDao {

    @Query("SELECT COUNT(*) FROM stock_parts")
    int count();

    @Query("SELECT * FROM stock_parts WHERE part_num=:partNum AND color_id=:colorId")
    List<Stock_parts> getByPartNumAndColorId(String partNum,int colorId);


    @Query("SELECT * FROM stock_parts WHERE part_num=:partNum")
    List<Stock_parts> getByPartNum(String partNum);

    @Insert
    long[] insertAll(Stock_parts... stock_parts);
}
