package com.example.mylego;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class TestAlertDialogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_alert_dialog);



    }

    public void afficherAlerte(View view){

        LayoutInflater factory = LayoutInflater.from(this);
        final View alertDialogView = factory.inflate(R.layout.alert_dialog_view, null);
        //Création de l'AlertDialog
        AlertDialog.Builder adb = new AlertDialog.Builder(this);

//On affecte la vue personnalisé que l'on a crée à notre AlertDialog
        adb.setView(alertDialogView);

//On affecte un bouton "OK" à notre AlertDialog et on lui affecte un évènement
        adb.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                //Lorsque l'on cliquera sur le bouton "OK", on récupère l'EditText correspondant à notre vue personnalisée (cad à alertDialogView)
                EditText valsaisie = (EditText)alertDialogView.findViewById(R.id.EditText);
                Toast.makeText(getApplicationContext(),"nouvelle valeur recuperere"+valsaisie.getText().toString(), Toast.LENGTH_LONG).show();
                //On affiche dans un Toast le texte contenu dans l'EditText de notre AlertDialog
                //Toast.makeText(this, valsaisie.getText(), Toast.LENGTH_SHORT).show();
            } });
        adb.show();
    }
}
