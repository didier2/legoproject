package com.example.mylego;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ThemesDao {
    @Query("SELECT COUNT(*) FROM themes")
    int count();

    @Query("SELECT * FROM themes")
    List<Themes> getAll();

    @Query("SELECT * FROM themes WHERE parent_id='NULL'")
    List<Themes> getPrincipaux();

    @Insert
    long[] insertAll(Themes... themes);
}
