<?php
include 'connect.php';

if (isset($_POST["import"])) {
    $table_name = $_POST["table"];
    
    $fileName = $_FILES["file"]["tmp_name"];
    
    if ($_FILES["file"]["size"] > 0) {
        
        $file = fopen($fileName, "r");
        $firstline = true;
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            if(!$firstline){
                if($table_name == "colors"){
                    $is_trans = $column[3]=='t';
                    $sqlInsert = "INSERT into colors (id,name,rgb,is_trans)
                           values ('" . $column[0] . "','" . $column[1] . "','" . $column[2] . "','" . $is_trans . "')";
                    #if($is_trans) echo $sqlInsert;
                    $result = mysql_query($sqlInsert);
                }else if($table_name == "inventories"){
                    $sqlInsert = "INSERT into inventories (id,version,set_num)
                           values ('" . $column[0] . "','" . $column[1] . "','" . $column[2] . "')";
                    $result = mysql_query($sqlInsert);
                }else if($table_name == "part_categories"){
                    $sqlInsert = "INSERT into part_categories (id,name)
                           values ('" . $column[0] . "','" . $column[1] . "')";
                    $result = mysql_query($sqlInsert);
                }
                if (! empty($result)) {
                    $type = "success";
                    $message = "CSV Data Imported into the Database";
                } else {
                    $type = "error";
                    $message = "Problem in Importing CSV Data";
                    echo mysql_error($conn)."<br/>";
                }
            }else{
                echo "firstline ignored";
                $firstline = false;
            }
        }
    }
}
?>
<!DOCTYPE html>
<html>

<head>

<style>
body {
	font-family: Arial;
	width: 550px;
}

.outer-scontainer {
	background: #F0F0F0;
	border: #e0dfdf 1px solid;
	padding: 20px;
	border-radius: 2px;
}

.input-row {
	margin-top: 0px;
	margin-bottom: 20px;
}

.btn-submit {
	background: #333;
	border: #1d1d1d 1px solid;
	color: #f0f0f0;
	font-size: 0.9em;
	width: 100px;
	border-radius: 2px;
	cursor: pointer;
}

.outer-scontainer table {
	border-collapse: collapse;
	width: 100%;
}

.outer-scontainer th {
	border: 1px solid #dddddd;
	padding: 8px;
	text-align: left;
}

.outer-scontainer td {
	border: 1px solid #dddddd;
	padding: 8px;
	text-align: left;
}

#response {
    padding: 10px;
    margin-bottom: 10px;
    border-radius: 2px;
    display:none;
}

.success {
    background: #c7efd9;
    border: #bbe2cd 1px solid;
}

.error {
    background: #fbcfcf;
    border: #f3c6c7 1px solid;
}

div#response.display-block {
    display: block;
}
</style>
</head>

<body>
    <h2>Import CSV file into Mysql using PHP</h2>
    
    <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    <div class="outer-scontainer">
        <div class="row">

            <form class="form-horizontal" action="" method="post"
                name="frmCSVImport" id="frmCSVImport" enctype="multipart/form-data">
                <div class="input-row">
                    <label class="col-md-4 control-label">Choose CSV
                        File</label> <input type="file" name="file"
                        id="file" accept=".csv"><br>
                    <label for="table" class="col-md-4 control-label">Choose table
                    </label>
                    <select name="table" id="table">
                        <option value="colors">colors</option>
                        <option value="inventories">inventories</option>
                        <option value="part_categories">part_categories</option>
                    </select>
                    <br>
                    <button type="submit" id="submit" name="import"
                        class="btn-submit">Import</button>
                    <br />

                </div>

            </form>

        </div>
    </div>

</body>

</html>