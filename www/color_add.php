<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset ="utf-8"/>
        <link rel="stylesheet" type="text/css" href="design/main.css">
        <title>Add color</title>

        <!--CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="design/main.css">

        <!--JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>

    <?php
        include 'nav.php';
        include 'bdd/connect.php';
        if (isset($_POST["form_add_color"])) {
            $color_bl = $_POST["code_bl"];
            $color_id = $_POST["color"];
            if($color_id != null){
                $query = " INSERT INTO `mylego`.`colors_code_id` (`colors_id` ,`bricklink_id`)VALUES ('$color_id', '$color_bl')";
                mysql_query($query);
                echo $query;
            }
        }
        $query = "select bricklink_id, id, name, rgb from colors left join colors_code_id on id = colors_id order by name asc;";
        $result = mysql_query($query);
    ?>

    <body>
        <div class="container-fluid">
            <div class="row" id="RowStyle">
                <div class="col-xs-4 offset-xs-4 col-sm-4 offset-sm-4">
                    <h1>Ajouter une couleur</h1><br>
                    <form action="color_add.php" method="post" name="form_add_color" id="form_add_color" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="color">Color: </label>
                            <select id="color" class="form-control">
                                <option selected>Choose...</option>?php
                                    <?php
                                        while ($row = mysql_fetch_assoc($result)) {
                                            $bl_id = $row['bricklink_id'];
                                            if($bl_id == null){
                                                $id = $row['id'];
                                                $name = $row['name'];
                                                echo '<option value="'.$id.'">'.$name.' '.$id.'</option>\n';
                                            }
                                        }
                                    ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="code_bl">Code Briklink: </label>
                            <input type="text" class="form-control" name="code_bl" id="code_bl" placeholder="ID">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div><br><br>

            <div class="row" id="RowStyle">
                <div class="col-xs-8 offset-xs-2 col-sm-8 offset-sm-2">
                    <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5">
                        <?php
                                mysql_data_seek($result, 0);
                                while ($row = mysql_fetch_assoc($result)) {
                                    $id = $row['id'];
                                    $bl_id = $row['bricklink_id'];
                                    $name = $row['name'];
                                    $color = $row['rgb'];
                                    echo "<div class='card' style='width: 18rem;'>";
                                    echo "   <img class='card-img-top' src='' height='150px' style='background-color:#".$color."'>";
                                    echo "   <div class='card-body'>";
                                    echo "       <h5 class='card-title'>".$name."</h5>";
                                    echo "   </div>";
                                    echo "   <ul class='list-group list-group-flush'>";
                                    echo "        <li class='list-group-item'>ID(bdd): ".$id."</li>";
                                    echo "        <li class='list-group-item'>ID(bricklink): ".$bl_id."</li>";
                                    echo "   </ul>";
                                    echo "</div>";
                                }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
