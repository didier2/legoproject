<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset ="utf-8"/>
        <link rel="stylesheet" type="text/css" href="design/main.css">
        <title>Add color</title>

        <!--CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="design/main.css">

        <!--JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>

    <?php
        include 'nav.php';
        include 'bdd/connect.php';
        if (isset($_POST["form_add_color"])) {
            $color_bl = $_POST["code_bl"];
            $color_id = $_POST["color"];
            if($color_id != null){
                $query = " INSERT INTO `mylego`.`colors_code_id` (`colors_id` ,`bricklink_id`)VALUES ('$color_id', '$color_bl')";
                mysql_query($query);
                echo $query;
            }
        }
        $query = "select bricklink_id, id, name, rgb from colors left join colors_code_id on id = colors_id order by name asc;";
        $result = mysql_query($query);
    ?>

    <body>
        <div class="container-fluid">
            <div class="row" id="RowStyle">
                <div class="col-xs-8 offset-xs-2 col-sm-8 offset-sm-2">
                    <h1>Couleurs en stock</h1><br>
                    <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5">
                        <?php
                                include 'utils/functions.php';
                                //Affichage normal
                                /*mysql_data_seek($result, 0);
                                while ($row = mysql_fetch_assoc($result)) {
                                    $id = $row['id'];
                                    $bl_id = $row['bricklink_id'];
                                    $name = $row['name'];
                                    $color = $row['rgb'];
                                    echo "<div class='card' style='width: 18rem;'>";
                                    echo "   <img class='card-img-top' src='' height='150px' style='background-color:#".$color."'>";
                                    echo "   <div class='card-body'>";
                                    echo "       <h5 class='card-title'>".$name."</h5>";
                                    echo "   </div>";
                                    echo "   <ul class='list-group list-group-flush'>";
                                    echo "        <li class='list-group-item'>ID(bdd): ".$id."</li>";
                                    echo "        <li class='list-group-item'>ID(bricklink): ".$bl_id."</li>";
                                    echo "   </ul>";
                                    echo "</div>";
                                }*/

                                
                                //Create an array of random RGB colours converted to HSV
                                mysql_data_seek($result, 0);
                                $s = array();
                                while ($row = mysql_fetch_assoc($result)) {
                                    $color = $row['rgb'];
                                    $r = substr($color, 0, 2);
                                    $g = substr($color, 2, 2);
                                    $b = substr($color, 4, 2);
                                    $s[] = rgbtohsv($r,$g,$b);
                                }

                                /*
                                echo sizeof($s);

                                for($j=0; $j<sizeof($s); $j++) {
                                    echo $s[$j][0]." ";
                                    echo $s[$j][1]." ";
                                    echo $s[$j][2];
                                    echo "<br>";
                                }*/
                                
                                //Split each array up into H, S and V arrays
                                foreach($s as $k => $v) {
                                    $hue[$k] = $v[0];
                                    $sat[$k] = $v[1];
                                    $val[$k] = $v[2];
                                }
                                
                                //Sort in ascending order by H, then S, then V and recompile the array
                                array_multisort($hue,SORT_ASC,$sat,SORT_ASC,$val,SORT_ASC,$s);
                                
                                //Display
                                foreach($s as $k => $v) {
                                    list($hue,$sat,$val) = $v;
                                    list($r,$g,$b) = hsvtorgb($hue,$sat,$val);
                                    echo "<div style='border:1px solid #000;padding:4px;background:rgb($r,$g,$b);'>HEX: ".sprintf("#%02x%02x%02x", $r, $g, $b)."\n RGB: $r,$g,$b</div>";
                                }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
