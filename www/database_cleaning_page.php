<!DOCTYPE html>
<html>
  <head>
      <meta http-equiv="Content-Type" content="text/html">
      <meta charset="utf-8" />
      <title>MyLego</title>
      <link rel="icon" href="design/logo.ico" />

      <!--CSS-->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <link rel="stylesheet" href="design/main.css">

      <!--JS-->
      <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>

  <?php
      include 'bdd/connect.php';
      include 'nav.php';
  ?>

  <body>
    <div class="container-fluid">
      <div class="row" id="RowStyle">
        <div class="col-xs-4 offset-xs-4 col-sm-4 offset-sm-4">
          <h1>Nettoyer la BDD</h1><br>
          <form action="" method="post" name="changenumber" id="changenumber">
            <p>Cliquez sur ce boutton pour vérifier votre base de donnée : <br>
              Ceci supprimera les pieces possédant des quantitées negatives (si il y en a) <br>
              ainsi qu'additionner les pieces de numéro et de couleur identiques, et surtout <br>
              supprimera les doublons.
            </p>

            <?php
            if (isset($_POST["clean"])) {include 'clean_database.php';}
            ?>
            <button type="submit" id="submit" name="clean" class="btn btn-primary">Vérification</button>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
