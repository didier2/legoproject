<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html">
    <meta charset="utf-8" />
    <title>MyLego</title>
    <link rel="icon" href="design/logo.ico" />

    <!--CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="design/main.css">

    <!--JS-->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>

<?php
    include 'bdd/connect.php';
    include 'nav.php';
?>
<html>
    <div class="container-fluid">
        <div class="row" id="RowStyle">
            <div class="col-xs-8 offset-xs-2 col-sm-8 offset-sm-2">
                        <h1>Quelques chiffres</h1>
                        <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5">
                            <div class="col mb-4">
                                <div class="card h-100">
                                    <img src="design/color.jpg" class="card-img-top" alt="..." height="125px">
                                    <div class="card-body">
                                        <h5 class="card-title">Couleurs</h5>
                                        <p class="card-text text-black-50"><?php
                                            $result = mysql_query("select count(*) as total from colors");
                                            $data=mysql_fetch_assoc($result);
                                            echo $data['total']." articles";
                                        ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-4">
                                <div class="card h-100">
                                    <img src="design/inventories.jpg" class="card-img-top" alt="..." height="125px">
                                    <div class="card-body">
                                        <h5 class="card-title">Inventaires</h5>
                                        <p class="card-text text-black-50"><?php
                                            $result = mysql_query("select count(*) as total from inventories");
                                            $data=mysql_fetch_assoc($result);
                                            echo $data['total']." articles";
                                        ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-4">
                                <div class="card h-100">
                                    <img src="design/inventories-parts.jpg" class="card-img-top" alt="..." height="125px">
                                    <div class="card-body">
                                        <h5 class="card-title">Inventaire des pièces</h5>
                                        <p class="card-text text-black-50"><?php
                                            $result = mysql_query("select count(*) as total from inventory_parts");
                                            $data=mysql_fetch_assoc($result);
                                            echo $data['total']." articles";
                                        ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-4">
                                <div class="card h-100">
                                    <img src="design/parts.jpg" class="card-img-top" alt="..." height="125px">
                                    <div class="card-body">
                                        <h5 class="card-title">Pièces</h5>
                                        <p class="card-text text-black-50"><?php
                                            $result = mysql_query("select count(*) as total from parts");
                                            $data=mysql_fetch_assoc($result);
                                            echo $data['total']." articles";
                                        ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-4">
                                <div class="card h-100">
                                    <img src="design/parts-categories.png" class="card-img-top" alt="..." height="125px">
                                    <div class="card-body">
                                        <h5 class="card-title">Catégories</h5>
                                        <p class="card-text text-black-50"><?php
                                            $result = mysql_query("select count(*) as total from part_categories");
                                            $data=mysql_fetch_assoc($result);
                                            echo $data['total']." articles";
                                        ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-4">
                                <div class="card h-100">
                                    <img src="design/sets.jpg" class="card-img-top" alt="..." height="125px">
                                    <div class="card-body">
                                        <h5 class="card-title">Collections</h5>
                                        <p class="card-text text-black-50"><?php
                                            $result = mysql_query("select count(*) as total from sets");
                                            $data=mysql_fetch_assoc($result);
                                            echo $data['total']." articles";
                                        ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-4">
                                <div class="card h-100">
                                    <img src="design/themes.jpg" class="card-img-top" alt="..." height="125px">
                                    <div class="card-body">
                                        <h5 class="card-title">Thèmes</h5>
                                        <p class="card-text text-black-50"><?php
                                            $result = mysql_query("select count(*) as total from themes");
                                            $data=mysql_fetch_assoc($result);
                                            echo $data['total']." articles";
                                        ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-4">
                                <div class="card h-100">
                                    <img src="design/missing-parts.jpg" class="card-img-top" alt="..." height="125px">
                                    <div class="card-body">
                                        <h5 class="card-title">Pièces manquantes</h5>
                                        <p class="card-text text-black-50"><?php
                                            $result = mysql_query("select sum(quantity) as total from missing_parts");
                                            $data=mysql_fetch_assoc($result);
                                            echo $data['total']." articles";
                                        ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-4">
                                <div class="card h-100">
                                    <img src="design/stock-parts.jpg" class="card-img-top" alt="..." height="125px">
                                    <div class="card-body">
                                        <h5 class="card-title">Pièces en stock</h5>
                                        <p class="card-text text-black-50"><?php
                                            $result = mysql_query("select sum(quantity) as total from stock_parts");
                                            $data=mysql_fetch_assoc($result);
                                            echo $data['total']." articles";
                                        ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col mb-4">
                                <div class="card h-100">
                                    <img src="design/sets-registered.jpg" class="card-img-top" alt="..." height="125px">
                                    <div class="card-body">
                                        <h5 class="card-title">Collections enregistrés</h5>
                                        <p class="card-text text-black-50"><?php
                                            $result = mysql_query("select count(*) as total from set_collection");
                                            $data=mysql_fetch_assoc($result);
                                            echo $data['total']." articles";
                                        ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-sm-4 offset-sm-1 col-xs-12">
                        <h3>Actions</h3>
                        <ul class="list-group list-group-flush bg-transparent">
                            <li class="list-group-item bg-transparent"><a href="set_search.php">Ajouter un set à la collection</a></li>
                            <li class="list-group-item bg-transparent"><a href="piece_search.php">Ajouter des pièces au vrac</a></li>
                            <li class="list-group-item bg-transparent"><a href="piece_changenumber.php">Changer des numéros de pièes</a></li>
                            <li class="list-group-item bg-transparent"><a href="color_add.php">Ajouter une couleur</a></li>
                            <li class="list-group-item bg-transparent"><a href="set_collection_see.php">Voir les sets en stock</a></li>
                            <li class="list-group-item bg-transparent"><a href="missing_part_see.php">Voir les pièces manquantes</a></li>
                            <li class="list-group-item bg-transparent"><a href="stock_part_see_categories.php">Voir les pièces en stock</a></li>
                            <li class="list-group-item bg-transparent">Cherche des pièces manquantes (inventory_id)<form class="form-horizontal" action="missing_part_search.php" method="get" name="form_search_missing_part" enctype="multipart/form-data"><input type="text" name="inventory_id"><button type="submit">Chercher</button></form></li>
                        </ul>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</html>
