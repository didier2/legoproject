<!DOCTYPE html>



<head>
    <meta http-equiv="Content-Type" content="text/html">
    <meta charset ="utf-8"/>
    <title>Search missing part</title>

    <!--CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="design/main.css">

    <!--JS-->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>

<html>
<?php
    include 'bdd/connect.php';
    include 'nav.php';

    if(isset($_POST["form_associate_missing_part"])){
        $missingID = $_POST['missingID'];
        $stockID = $_POST['stockID'];
        $query="UPDATE mylego.stock_parts SET quantity = quantity-1 WHERE id =$stockID";
        $result = mysql_query($query);
        if($result){
          $query = "SELECT parts.name, part_categories.name as categorie from parts, part_categories where parts.part_cat_id = part_categories.id and parts.part_num =(SELECT part_num from stock_parts where id =$stockID) ";
          $confirmation = mysql_query($query);
          $result = mysql_fetch_row($confirmation);
          echo "Vous avez bien ajouter la piece : ", $result[0]," de catégorie : ", $result[1], "<br>";
          $query="UPDATE mylego.missing_parts SET quantity = quantity-1 WHERE id =$missingID";
          mysql_query($query);
        }


    }
    if(isset($_GET['inventory_id'])){
        $inventory_id = $_GET['inventory_id'];
        $query = "SELECT mp.id as missingID, mp.part_num, c1.name as missingColorName, c1.rgb as missingColorRGB, cc1.bricklink_id as color_bl, mp.quantity as missingQtty, sp.id as stockID, sp.quantity as stockQtty, c2.name as stockColorName, c2.rgb as stockColorRGB
        FROM missing_parts as mp
        left join colors as c1 on c1.id=color_id
        left join colors_code_id as cc1 on c1.id = cc1.colors_id
        join stock_parts as sp on mp.part_num=sp.part_num
        left join colors as c2 on c2.id=sp.color_id
        where mp.inventory_id = '$inventory_id' and mp.quantity > 0 and sp.quantity > 0";
        $result_same_color = mysql_query("$query and c1.id=c2.id;");
        $query = "$query and c1.id!=c2.id;";
    }else if(isset($_GET['part_num'])){
        $part_num = $_GET['part_num'];
        $query = "SELECT mp.id as missingID, mp.part_num, c1.name as missingColorName, c1.rgb as missingColorRGB, cc1.bricklink_id as color_bl, mp.quantity as missingQtty, sp.id as stockID, sp.quantity as stockQtty, c2.name as stockColorName, c2.rgb as stockColorRGB
        FROM missing_parts as mp
        left join colors as c1 on c1.id=color_id
        join stock_parts as sp on mp.part_num=sp.part_num
        left join colors as c2 on c2.id=sp.color_id
        left join colors_code_id as cc1 on c2.id = cc1.colors_id
        where sp.part_num = '$part_num';";
    }
    $result = mysql_query($query);
?>

<body>
    <h1>Résultat de la recherche</h1><br>
    <table align="center">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th colspan="2">missing</th>
                <th></th>
                <th colspan="2">stock</th>
                <th></th>
            </tr>
            <tr>
                <th></th>
                <th>part_num</th>
                <th>color</th>
                <th>quantity</th>
                <th></th>
                <th>color</th>
                <th>quantity</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(isset($_GET['inventory_id'])){
                    while ($row = mysql_fetch_assoc($result_same_color)) {
                        echo '<tr align="center">';
                        $part_num = $row['part_num'];
                        $color_bl = $row['color_bl'];
                        $image = 'https://img.bricklink.com/ItemImage/PN/'.$color_bl.'/'.$part_num.'.png';
                        $missingColorName = $row['missingColorName'];
                        $missingColorRGB = $row['missingColorRGB'];
                        $missingQtty = $row['missingQtty'];
                        $missingID = $row['missingID'];
                        $stockColorName = $row['stockColorName'];
                        $stockColorRGB = $row['stockColorRGB'];
                        $stockQtty = $row['stockQtty'];
                        $stockID = $row['stockID'];
                        echo '<td><a href="javascript:PopupImage(\''.$image.'\')"><img style="max-width: 80px;" src="'.$image.'"></a></td>';
                        echo '<td><a href="https://www.bricklink.com/v2/catalog/catalogitem.page?P='.$part_num.'&idColor='.$color_bl.'">'.$part_num.'</a></td>';
                        echo '<td style="border-width:1px; background-color:#'.$missingColorRGB.'"><span style="background-color:#FFFFFF;" >'.$missingColorName.'</span></td>';
                        echo "<td>".$missingQtty."</td>";
                        echo "<td></td>";
                        echo '<td style="border-width:1px; background-color:#'.$stockColorRGB.'"><span style="background-color:#FFFFFF;" >'.$stockColorName.'</span></td>';
                        echo "<td>".$stockQtty."</td>";
                        if($stockQtty>0){
                            echo '<td><form class="form-horizontal" action="missing_part_search.php?';
                            if($inventory_id!=null){
                                echo 'inventory_id='.$inventory_id.'"';
                            }else {
                                echo 'part_num='.$part_num.'"';
                            }
                            echo 'method="post" name="form_associate_missing_part" enctype="multipart/form-data">
                            <input type="hidden" name="missingID" value="'.$missingID.'">
                            <input type="hidden" name="stockID" value="'.$stockID.'">
                            <button name="form_associate_missing_part" class="btn-submit" type="submit">Associer</button></form></td>';
                        }else{
                            echo "<td></td>";
                        }
                        echo "</tr>\n";
                    }
                }
                while ($row = mysql_fetch_assoc($result)) {
                    echo '<tr align="center">';
                    $part_num = $row['part_num'];
                    $color_bl = $row['color_bl'];
                    $image = 'https://img.bricklink.com/ItemImage/PN/'.$color_bl.'/'.$part_num.'.png';
                    $missingColorName = $row['missingColorName'];
                    $missingColorRGB = $row['missingColorRGB'];
                    $missingQtty = $row['missingQtty'];
                    $missingID = $row['missingID'];
                    $stockColorName = $row['stockColorName'];
                    $stockColorRGB = $row['stockColorRGB'];
                    $stockQtty = $row['stockQtty'];
                    $stockID = $row['stockID'];
                    echo '<td><a href="javascript:PopupImage(\''.$image.'\')"><img style="max-width: 80px;" src="'.$image.'"></a></td>';
                    echo '<td><a href="https://www.bricklink.com/v2/catalog/catalogitem.page?P='.$part_num.'&idColor='.$color_bl.'">'.$part_num.'</a></td>';
                    echo '<td style="border-width:1px; background-color:#'.$missingColorRGB.'"><span style="background-color:#FFFFFF;" >'.$missingColorName.'</span></td>';
                    echo "<td>".$missingQtty."</td>";
                    echo "<td></td>";
                    echo '<td style="border-width:1px; background-color:#'.$stockColorRGB.'"><span style="background-color:#FFFFFF;" >'.$stockColorName.'</span></td>';
                    echo "<td>".$stockQtty."</td>";
                    if($stockQtty>0){
                        echo '<td><form class="form-horizontal" action="missing_part_search.php?';
                        if($inventory_id!=null){
                            echo 'inventory_id='.$inventory_id.'"';
                        }else {
                            echo 'part_num='.$part_num.'"';
                        }
                        echo 'method="post" name="form_associate_missing_part" enctype="multipart/form-data">
                        <input type="hidden" name="missingID" value="'.$missingID.'">
                        <input type="hidden" name="stockID" value="'.$stockID.'">
                        <button name="form_associate_missing_part" class="btn-submit" type="submit">Associer</button></form></td>';
                    }else{
                        echo "<td></td>";
                    }
                    echo "</tr>\n";
                }
            ?>
        </tbody>
    </table>
</body>

</html>
