
<nav class="navbar navbar-expand-xl navbar-light bg-dark py-lg-4">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto navbar-center">
            <li class="nav-item">
                <a class="nav-link text-white" href="home.php">Accueil</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Voir le stock</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="color_see.php">Les couleurs</a>
                <a class="dropdown-item" href="stock_part_see_categories.php">Mon stock des pièces</a>
                <a class="dropdown-item" href="set_collection_see.php">Mes sets</a>

            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Chercher</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="piece_search.php">Une pièce</a>
                <a class="dropdown-item" href="set_search.php">Un set</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ajouter</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="color_add.php">Une couleur</a>
                <a class="dropdown-item" href="piece_add.php">Une pièce</a>
                <a class="dropdown-item" href="set_add.php">Un set</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Changer</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="piece_changenumber.php">Les numéros d'une pièce</a>
                <a class="dropdown-item" href="piece_changecolor.php">La couleur d'une pièce</a>
            </li>
            <li>
              <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Divers</a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="missing_part_see.php">Les pièces manquantes</a>
              <a class="dropdown-item" href="parts_see_categorie.php">L'ensemble des pieces</a>
              <a class="dropdown-item" href="set_categories_see.php">L'ensemble des sets</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="database_cleaning_page.php">Nettoyer BDD</a>
            </li>
        </ul><!--
        <form class=" my-2 my-lg-0" action="missing_part_search.php" method="get" name="form_search_missing_part" enctype="multipart/form-data">
            <input class="form-control mr-sm-2" type="search" name="inventory_id" placeholder="Identifiant">
            <button  class="btn btn-outline-white my-2 my-sm-0 bg-info text-white" type="submit">Chercher pièces manquantes</button>
        </form>-->
    </div>
</nav>
<br><br><br><br>
