<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset ="utf-8"/>
        <link rel="stylesheet" type="text/css" href="style/form.css">
        <title>See parts</title>
        <!--CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="design/main.css">

        <!--JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <style>
          #input_addPiece{
            width: 5em;
          }
        </style>
    </head>

    <script type='text/javascript'>
        function PopupImage(img) {
            w = open("", 'image', 'weigth=toolbar=no,scrollbars=no,resizable=yes, width=510, height=210');
            w.document.write("<html>");
            w.document.write("<script type='text/javascript'>function checksize() { window.resizeTo(document.images[0].width+10,document.images[0].height+35);window.focus(); } <\/script>");
            w.document.write("<body onload='checksize()' onblur='window.close()' onclick='window.close()' topmargin=0 leftmargin=0 marginwidth=0 marginheight=0>");
            w.document.write("<img src='" + img + "' border='0' alt='image' />");
            w.document.write("</body></html>");
            w.document.close();
        }

    </script>

    <?php
        include 'nav.php';
        include 'bdd/connect.php';
        $cat_id = $_GET['cat_id'];
        $cat_name = $_GET['cat_name'];
        if(isset($cat_id)){
            $query = "SELECT p.part_num, p.name
                      FROM parts as p
                      where p.part_cat_id = $cat_id;";
        }else{
            $query = "SELECT p.part_num, p.name, part_categories.name
                      FROM parts as p, part_categories as pc
                      where p.part_cat_id = pc.id;";
        }


        $result =  mysql_query($query);
    ?>

    <body>
        <div class="container-fluid">
            <div class="row" id="RowStyle">
                <div class="col-xs-8 offset-xs-2 col-sm-8 offset-sm-2">
                    <h1>
                        <?php echo $cat_name;?>
                    </h1><br>
                    <div class="table-responsive-lg">
                        <table class="table table-hover table-light">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>part_num</th>
                                    <th>name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    while ($row = @mysql_fetch_assoc($result)) {
                                        echo '<tr scope="row">';
                                        $part_num = $row['part_num'];
                                        $name = $row['name'];
                                        $image = 'https://img.bricklink.com/ItemImage/PN/11/'.$part_num.'.png';
                                        echo '<td><a href="javascript:PopupImage(\''.$image.'\')"><img style="max-width: 80px;" src="'.$image.'"></a></td>';
                                        echo '<td>'.$part_num.'</td>';
                                        echo '<td>'.$name.'</td>';
                                        echo '<td><a href="piece_add.php?piece_num='.$part_num.'"><button type="submit" id="addPiece" name="addPiece" class="btn-submit">Add</button></a></td>';
                                        //echo '<td><a href="https://www.bricklink.com/v2/catalog/catalogitem.page?P='.$part_num.'&colorName='.$color_bl.'">'.$part_num.'</a><input type="hidden" name="part_num[]" value="'.$part_num.'"></td>';
                                        echo "</tr>\n";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
