<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset ="utf-8"/>
        <link rel="stylesheet" type="text/css" href="style/table.css">
        <link rel="stylesheet" type="text/css" href="style/form.css">
        <title>Add piece</title>

        <!--CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="design/main.css">

        <!--JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>

    <?php
        include 'nav.php';
        include 'bdd/connect.php';
        include 'utils/functions.php';
        @$piece_num = trim($_GET['piece_num']);

        if (isset($_POST["addpiece"])) {
            $quantity = $_POST["quantity"];
            @$notsure = $_POST["notsure"];
            $query_searched = "select count(*) as total from missing_parts where part_num='$piece_num' and quantity>0";
            if($notsure){
                $query = "INSERT INTO mylego.stock_parts (`part_num`, `quantity`) VALUES ('$piece_num', $quantity);";
            }else{
                $color_id = $_POST["color"];
                $query = "INSERT INTO mylego.stock_parts (`part_num`, color_id, `quantity`) VALUES ('$piece_num', $color_id, $quantity);";
                $query_searched .=" and color_id =$color_id";
            }
            $result = mysql_query($query);
        }

        $query_colors = "select id, name, rgb from colors order by name asc;";
        $result_colors = mysql_query($query_colors);
    ?>

    <body>
        <h1>Ajouter une pièce</h1>
        <div class="outer-scontainer form_add">
            <?php
                if (isset($_POST["addpiece"])) {
                    $result = mysql_query($query_searched);
                    $data=mysql_fetch_assoc($result);
                    if($data['total']>0){
                        echo 'Cette pièce est recherchée <a href="missing_part_see.php?part_num='.$part_num.'">See missing pieces</a>';
                    }
                }
            ?>
            <h3>La piece
                <?php echo $piece_num;?>
                sera ajouté
            </h3>
            <p>
                <img style="max-width: 80px;" src="https://img.bricklink.com/ItemImage/PN/3/<?php echo $piece_num;?>.png">
                <img style="max-width: 80px;" src="https://img.bricklink.com/ItemImage/PN/7/<?php echo $piece_num;?>.png">
                <img style="max-width: 80px;" src="https://img.bricklink.com/ItemImage/PN/1/<?php echo $piece_num;?>.png">
                <img style="max-width: 80px;" src="https://img.bricklink.com/ItemImage/PN/11/<?php echo $piece_num;?>.png">
            </p>
            <form class="form-horizontal" action="piece_add.php?piece=<?php echo $piece_num;?>" method="post" name="addpiece" id="addpiece" enctype="multipart/form-data">
                <label class="col-md-4 control-label">Nombre de pièces :</label>
                <input type="text" name="quantity" id="quantity" value="1"><br>
                <label class="col-md-4 control-label">Couleur :</label>
                <select name="color" id="color">
                    <option value="0">Black</option>
                    <option value="1">Blue</option>
                    <option value="7">Light Gray</option>
                    <option value="8">Dark Gray</option>
                    <option value="4">Red</option>
                    <option value="14">Yellow</option>
                    <option value="15">White</option>
                    <?php
                    $isHead = isPieceHead($piece_num);
                    while ($row = mysql_fetch_assoc($result_colors)) {
                        $id = $row['id'];
                        $name = $row['name'];
                        $color = $row['rgb'];
                        if($isHead && ($id==14)){#yellow
                            echo '<option value="'.$id.'" selected="selected">'.$name.'</option>\n';
                        /*}else if($id==0){#yellow
                            echo '<option value="'.$id.'" selected="selected">'.$name.'</option>\n';*/
                        }else{
                            echo '<option value="'.$id.'">'.$name.'</option>\n';
                        }
                    }
                ?>
                </select>
                <input type="checkbox" id="notsure" name="notsure">
                <label>not sure ?</label><br>
                <button type="submit" id="addpiece" name="addpiece" class="btn-submit">Add piece</button>
            </form>
        </div>

        <div class="row" id="RowStyle">
            <div class="col-xs-8 offset-xs-2 col-sm-8 offset-sm-2">
                <h3>Ajouts récent :</h3><br>
                <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5">
                    <?php
                        include 'piece_added.php';
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>
