<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html">
    <meta charset ="utf-8"/>
    <title>Add Piece</title>

    <!--CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="design/main.css">

    <!--JS-->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<?php
    include 'nav.php';
    include 'bdd/connect.php';
    $part_num = $_GET['piece'];

    if (isset($_POST["addpiece"])) {
        $quantity = $_POST["quantity"];
        $notsure = $_POST["notsure"];
        $query_searched = "select count(*) as total from missing_parts where part_num='$part_num' and quantity>0";
        if($notsure){
            $query = "INSERT INTO mylego.stock_parts (`part_num`, `quantity`) VALUES ('$part_num', $quantity);";
        }else{
            $color_id = $_POST["color"];
            $query = "INSERT INTO mylego.stock_parts (`part_num`, color_id, `quantity`) VALUES ('$part_num', $color_id, $quantity);";
            $query_searched .=" and color_id =$color_id";
        }
        $result = mysql_query($query);
    }
?>

<body>
    <?php
        $result = mysql_query($query_searched);
        $data=mysql_fetch_assoc($result);
        if($data['total']>0){
            echo 'Cette pièce est recherchée <a href="missing_part_see.php?part_num='.$part_num.'">See missing pieces</a>';
        }else{
            echo "<script type='text/javascript'>document.location.replace('piece_search.php');</script>";
        }
    ?>
</body>

</html>
