<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset ="utf-8"/>
        <title>Change number</title>

        <!--CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="design/main.css">

        <!--JS-->
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <style>
        /* à mettre dans le bon fichier css */
        .ui-menu .ui-menu-item a{
          background:red;
          height:10px;
          font-size:8px;
        }
        .ui-helper-hidden-accessible { display:none; }

        ul.ui-autocomplete {
            list-style: none;
            background-color: white;
            border: 1px solid black;
            border-radius: 8px;
            width : 20em;
        }
        </style>
    </head>

    <?php
        include 'nav.php';
        include 'bdd/connect.php';
        $query_color_names = "select name from colors;";
        $result_color_names = mysql_query($query_color_names);
        $colorName = mysql_fetch_row($result_color_names);
        $listColorNames = array();
        while($colorName != false){
          array_push($listColorNames, $colorName[0]);
          $colorName = mysql_fetch_row($result_color_names);
        }
    ?>

    <div class="container-fluid">
        <div class="row" id="RowStyle">
            <div class="col-xs-4 offset-xs-4 col-sm-4 offset-sm-4">
              <h1>Changer la couleur d'une pièce</h1><br>
                <form action="" method="post" name="changenumber" id="changenumber" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Référence De La Piece</label>
                        <input type="text" class="form-control" name="number" id="number" placeholder="58176">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Nom De L'Ancienne Couleur</label>
                        <input type="text" class="form-control" name="oldColor" id="oldColor" placeholder="Light Turquoise">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput3">Nom De La Nouvelle Couleur</label>
                        <input type="text" class="form-control" name="newColor" id="newColor" placeholder="Dark Blue-Violet">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput4">Quantité à Changer</label>
                        <input type="number" class="form-control" name="quantity" id="quantity" placeholder="0">
                    </div>
                    <?php
                        if (isset($_POST["change"])) {
                          //partie declaration et recuperation valeur des variables
                            $pieceNumber = $_POST["number"];
                            $pieceNewColor = $_POST["newColor"];
                            $pieceOldColor = $_POST["oldColor"];
                            $Quantity = $_POST["quantity"];
                            // id de la couleur actuelle
                            $query = "select id from colors where name ='$pieceOldColor'; ";
                            $result = mysql_query($query);
                            $old_color_id = mysql_fetch_row($result);
                            //ecriture de la requete (query) + stockage dans variable
                            //execution de la requete + stockage resultat (ressource type mysql_query)
                            //lecture de la ressource + stockage dans variable
                            // id de la couleur finale
                            $query = "select id from colors where name ='$pieceNewColor'; ";
                            $result = mysql_query($query);
                            $new_color_id = mysql_fetch_row($result);
                            // quantité de piece cible  -> creation ou incrémentation
                            $query = "select quantity from stock_parts where part_num='$pieceNumber' and color_id ='$new_color_id[0]';";
                            $result = mysql_query($query);
                            $actualNewQuantity = mysql_fetch_row($result);
                            // quantité de la piece à modifier -> autorisation ou non
                            $query = "select quantity from stock_parts where part_num='$pieceNumber' and color_id ='$old_color_id[0]';";
                            $result = mysql_query($query);
                            $actualOldQuantity = mysql_fetch_row($result);
                            //si la piece à modifier n'existe pas -> erreur
                            if($actualOldQuantity[0] == NULL){
                              echo '<p> Erreur, la piece à modifier n\'existe pas </p>';
                            } else {
                              //si la quantité de piece cible demandé est superieur au stock de piece à modifier -> erreur
                              if($Quantity > $actualOldQuantity[0]){
                                echo "<p> Erreur, la quantité souhaité ($Quantity) Est
                                superieur au stock de pieces à modifier ($actualOldQuantity[0]) </p>";
                              } else {
                                //si la nouvelle piece n'existe pas -> création
                                //sinon modification du nombre de pieces actuelle
                                if($actualNewQuantity[0] == NULL or 0){
                                  $query="insert into stock_parts values ('select MAX(id) from stock_parts','$pieceNumber','$new_color_id[0]','$Quantity') ;";
                                  mysql_query($query);
                                } else {
                                  $query = "update stock_parts set quantity= quantity + $Quantity where part_num = $pieceNumber and color_id =$new_color_id[0];";
                                  mysql_query($query);
                                }
                                //mise à jour de la quantité de l'ancienne piece
                                $query = "update stock_parts set quantity = quantity -$Quantity where part_num = $pieceNumber and color_id = $old_color_id[0];";
                                mysql_query($query);
                              }
                            }
                            echo "<span class='bg-danger text-light'>Lignes inventory_parts modifiées : ".mysql_affected_rows()."</span>";
                            echo"</br>";
                        }

                    ?>
                    <button type="submit" id="submit" name="change" class="btn btn-primary">Changer</button>
                </form>
            </div>
        </div>
        <script>
        var passedArray =  <?php echo json_encode($listColorNames); ?>;
        var availableTags = passedArray;
        //il ne reste plus qu'a récuperer $listColorNames et le passer au js pour remplacer availableTags
      	$("#oldColor").autocomplete({
      		source: availableTags
      	});
        $("#newColor").autocomplete({
      		source: availableTags
      	});
      </script>
    </div>
</html>
