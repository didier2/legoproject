<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset ="utf-8"/>
        <link rel="stylesheet" type="text/css" href="style/table.css">
        <link rel="stylesheet" type="text/css" href="style/form.css">
        <title>Search Piece</title>

        <!--CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="design/main.css">

        <!--JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>

    <body>
        <?php
            include 'nav.php';
            include 'bdd/connect.php';
        ?>
        <div class="container-fluid">
            <div class="row" id="RowStyle">
                <div class="col-xs-4 offset-xs-4 col-sm-4 offset-sm-4">
                    <h1>Chercher une pièce</h1><br>
                    <div class="outer-scontainer">
                        <?php
                            if (isset($_POST["chercher"])) {
                                include 'piece_select.php';
                            }
                        ?>
                    </div>

                    <form action="" method="post" name="ajoutpiece" id="ajoutpiece" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="piecenumber">Nom ou Numéro de la pièce :</label>
                            <input class="form-control" type="text" name="piecenumber" id="piecenumber"><br>
                            <button type="submit" id="submit" name="chercher" class="btn btn-primary">Chercher</button>
                        </div>
                    </form>
                </div>
            </div><br>

            <div class="row" id="RowStyle">
                <div class="col-xs-8 offset-xs-2 col-sm-8 offset-sm-2">
                <h3>Ajouts récent :</h3><br>
                    <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5">
                        <?php
                            include 'piece_added.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
