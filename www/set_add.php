<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html">
    <meta charset ="utf-8"/>
    <link rel="stylesheet" type="text/css" href="style/form.css">
    <title>Add Set</title>
    <!--CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="design/main.css">

    <!--JS-->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<script type='text/javascript'>
    function PopupImage(img) {
        w = open("", 'image', 'weigth=toolbar=no,scrollbars=no,resizable=yes, width=510, height=210');
        w.document.write("<html>");
        w.document.write("<script type='text/javascript'>function checksize() { window.resizeTo(document.images[0].width+10,document.images[0].height+35);window.focus(); } <\/script>");
        w.document.write("<body onload='checksize()' onblur='window.close()' onclick='window.close()' topmargin=0 leftmargin=0 marginwidth=0 marginheight=0>");
        w.document.write("<img src='" + img + "' border='0' alt='image' />");
        w.document.write("</body></html>");
        w.document.close();
    }

</script>
<?php
    include 'nav.php';
    include 'bdd/connect.php';
    @$set_num = $_GET['set'];
    $query = "select id from inventories where set_num like '$set_num%';";
    $result = mysql_query($query);
    $data = mysql_fetch_assoc($result);
    $inventory_id = $data['id'];
    $query = "SELECT `part_num`,`rgb`,`quantity`,`is_spare`, bricklink_id as color_bl, color.name as colorname, color.id as color_id, inventory_id
    FROM inventory_parts as ip
    join colors as color on color.id=ip.color_id
    left join colors_code_id on color.id = colors_code_id.colors_id
    where ip.inventory_id ='$inventory_id' order by color.name, part_num, quantity desc;";
        $result = mysql_query($query);
?>

<body>
    <h1>Ajouter un set</h1><br>
    <div class="stayonleft">
        <a href="https://www.bricklink.com/catalogItemInv.asp?S=<?php echo $set_num;?>">Bricklink</a>
    </div>
    <form class="form-horizontal" action="set_add_process.php?set_num=<?php echo $set_num;?>" method="post" name="addset" id="addset" enctype="multipart/form-data">
        <input type="hidden" name="inventory_id" value="<?php echo $inventory_id;?>">
        <table align="center">
            <thead>
                <tr>
                    <th></th>
                    <th>part_num</th>
                    <th>color</th>
                    <th>quantity</th>
                    <th>is_spare</th>
                    <th>missing</th>
                </tr>
            </thead>
            <tbody>
                <?php
                while ($row = mysql_fetch_assoc($result)) {
                    echo '<tr align="center">';
                    $part_num = $row['part_num'];
                    $color_bl = $row['color_bl'];
                    $image = 'https://img.bricklink.com/ItemImage/PN/'.$color_bl.'/'.$part_num.'.png';
                    $quantity = $row['quantity'];
                    echo '<td><a href="javascript:PopupImage(\''.$image.'\')"><img style="max-width: 80px;" src="'.$image.'"></a></td>';
                    echo '<td><a href="https://www.bricklink.com/v2/catalog/catalogitem.page?P='.$part_num.'&idColor='.$color_bl.'">'.$part_num.'</a><input type="hidden" name="part_num[]" value="'.$part_num.'"></td>';
                    echo '<td style="border-width:1px; background-color:#'.$row['rgb'].'"><input type="hidden" name="color_id[]" value="'.$row['color_id'].'"><span style="background-color:#FFFFFF;" >'.$row['colorname'].'</span></td>';
                    echo "<td>".$quantity."</td>";
                    echo "<td>".$row['is_spare']."</td>";
                    echo '<td><select name="missing[]">';
                    for($i = 0; $i<=$quantity; $i++){
                        echo '<option value="'.$i.'">'.$i.'</option>"';
                    }
                    echo "</select></td>";
                    echo "</tr>\n";
                }
            ?>
            </tbody>
        </table>
        <button type="submit" id="addset" name="addset" class="btn-submit">Add set</button>
    </form>
</body>

</html>
