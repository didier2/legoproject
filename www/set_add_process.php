<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html">
    <meta charset ="utf-8"/>
    <title>Adding Set</title>
    <!--CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="design/main.css">

    <!--JS-->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>

<body>
    <?php
    include 'nav.php';
    include 'bdd/connect.php';
    $inventory_id = $_POST['inventory_id'];

    if (isset($_POST["addset"])) {
        $missing = $_POST["missing"];
        $part_num = $_POST["part_num"];
        $color_id = $_POST["color_id"];
        $query = "INSERT INTO set_collection (inventory_id) VALUES ($inventory_id)";
        mysql_query($query);
        for($i=0;$i<count($missing);$i++){
            if($missing[$i]>0){
                $query = "INSERT INTO missing_parts (inventory_id, part_num, color_id, quantity) VALUES ($inventory_id, '$part_num[$i]', '$color_id[$i]', '$missing[$i]')";
                mysql_query($query);
             printf("Lignes ajoutées %d. qtté : $missing[$i] - $part_num[$i]<br>\n", mysql_affected_rows());
            }
        }
    }
    ?>
    <a href="missing_part_search.php?inventory_id=<?php echo $inventory_id;?>">See pieces in stock</a>
</body>

</html>
