<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset ="utf-8"/>
        <title>See Collection</title>
        <!--CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="design/main.css">

        <!--JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>

    <?php
        include 'nav.php';
        include 'bdd/connect.php';
        $setnumber = @$_POST["setnumber"];
        $query = "SELECT inv.set_num, sets.name as set_name, year, themes.name as theme_name, count(mp.quantity) as qtty_missing
        FROM set_collection as setcol
        join inventories as inv on setcol.inventory_id = inv.id
        join sets on inv.set_num = sets.set_num
        join themes on sets.theme_id = themes.id
        left join missing_parts as mp on mp.inventory_id = inv.id
        group by set_num;";
        $result = @mysql_query($query);
    ?>

    <script type='text/javascript'>
        function PopupImage(img) {
            w = open("", 'image', 'weigth=toolbar=no,scrollbars=no,resizable=yes, width=510, height=210');
            w.document.write("<html>");
            w.document.write("<script type='text/javascript'>function checksize() { window.resizeTo(document.images[0].width+10,document.images[0].height+35);window.focus(); } <\/script>");
            w.document.write("<body onload='checksize()' onblur='window.close()' onclick='window.close()' topmargin=0 leftmargin=0 marginwidth=0 marginheight=0>");
            w.document.write("<img src='" + img + "' border='0' alt='image' />");
            w.document.write("</body></html>");
            w.document.close();
        }
    </script>

    <div class="container-fluid">
            <div class="row" id="RowStyle">
                <div class="col-xs-8 offset-xs-2 col-sm-8 offset-sm-2">
                    <body>
                        <h1>Collection en stock</h1><br>
                        <div class="table-responsive-lg">
                            <table class="table table-hover table-light">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>set_num</th>
                                        <th>set_name</th>
                                        <th>year</th>
                                        <th>theme_name</th>
                                        <th>qtty missing</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                            while ($row = mysql_fetch_assoc($result)) {
                                                echo "<tr scope='row' style=\"cursor: pointer;\">";
                                                $set_num = $row['set_num'];
                                                $image = 'https://img.bricklink.com/ItemImage/SN/0/'.$set_num.'.png';
                                                echo '<td><a href="javascript:PopupImage(\''.$image.'\')"><img style="max-width: 80px;" src="'.$image.'"></a></td>';
                                                echo "<td>".$set_num."</td>";
                                                echo "<td>".$row['set_name']."</td>";
                                                echo "<td>".$row['year']."</td>";
                                                echo "<td>".$row['theme_name']."</td>";
                                                echo "<td>".$row['qtty_missing']."</td>";
                                                echo "</tr>";
                                            }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </body>
                </div>
            </div>
        </div>
    </div>
</html>
