<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset ="utf-8"/>
        <link rel="stylesheet" type="text/css" href="style/form.css">
        <title>See set completion</title>
        <!--CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="design/main.css">

        <!--JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <style>
          #input_addPiece{
            width: 5em;
          }
        </style>
    </head>

    <script type='text/javascript'>
        function PopupImage(img) {
            w = open("", 'image', 'weigth=toolbar=no,scrollbars=no,resizable=yes, width=510, height=210');
            w.document.write("<html>");
            w.document.write("<script type='text/javascript'>function checksize() { window.resizeTo(document.images[0].width+10,document.images[0].height+35);window.focus(); } <\/script>");
            w.document.write("<body onload='checksize()' onblur='window.close()' onclick='window.close()' topmargin=0 leftmargin=0 marginwidth=0 marginheight=0>");
            w.document.write("<img src='" + img + "' border='0' alt='image' />");
            w.document.write("</body></html>");
            w.document.close();
        }

    </script>

    <?php
        include 'nav.php';
        include 'bdd/connect.php';
        @$theme_id = $_GET['theme_id'];
        @$theme_name = $_GET['theme_name'];

        if(isset($theme_id)){
            $query = "SELECT sets.set_num, sets.name, sets.year, themes.name
                      FROM sets, themes
                      WHERE sets.theme_id = themes.id  and sets.theme_id = $theme_id";
        }else{
          $query = "SELECT sets.set_num, sets.name, sets.year, themes.name
                    FROM sets, themes
                    WHERE sets.theme_id = themes.id  order by themes.name";
        }
        $result = @mysql_query($query);
    ?>

    <body>
        <div class="container-fluid">
            <div class="row" id="RowStyle">
                <div class="col-xs-8 offset-xs-2 col-sm-8 offset-sm-2">
                    <h1>
                        <?php echo $theme_name;?>
                    </h1><br>
                    <div class="table-responsive-lg">
                      <table class="table table-hover table-light">
                          <thead>
                              <tr>
                                  <th></th>
                                  <th>set_num</th>
                                  <th>set_name</th>
                                  <th>year</th>
                                  <th>qtty missing</th>
                                  <th>percentage of faisability</th>
                              </tr>
                          </thead>
                          <tbody>
                                  <?php
                                      while ($row = mysql_fetch_assoc($result)) {
                                          echo "<tr scope='row' style=\"cursor: pointer;\">";
                                          $set_num = $row['set_num'];
                                          $image = 'https://img.bricklink.com/ItemImage/SN/0/'.$set_num.'.png';
                                          echo '<td><a href="javascript:PopupImage(\''.$image.'\')"><img style="max-width: 80px;" src="'.$image.'"></a></td>';
                                          echo "<td>".$set_num."</td>";
                                          echo "<td>".$row['name']."</td>";
                                          echo "<td>".$row['year']."</td>";
                                          //echo "<td>".$row['qtty_missing']."</td>";
                                          echo "</tr>";
                                      }
                                  ?>
                          </tbody>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
