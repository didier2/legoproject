<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset ="utf-8"/>
        <link rel="stylesheet" type="text/css" href="design/main.css">
        <title>Sets Progression</title>

        <!--CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="design/main.css">

        <!--JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>
    <?php
      include 'nav.php';
      include 'bdd/connect.php';
      $query = "SELECT name, id FROM themes WHERE parent_id is NULL;";
      $result = mysql_query($query);
    ?>
    <body>
      <h1>Complétion actuelle des sets</h1>
      <div class="container-fluid">
          <div class="row" id="RowStyle">
              <div class="col-xs-8 offset-xs-2 col-sm-8 offset-sm-2">
                  <h1>Pièces en stock</h1><br>
                  <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5">
                      <?php
                          $i =0;
                          while ($row = mysql_fetch_assoc($result)) {
                              $i++;
                              $theme_name = $row['name'];
                              $theme_id = $row['id'];
                              //$quantity = $row['qtty'];
                              echo "<div class='card' style='width: 18rem;'>";
                              echo "   <img class='card-img-top' src='design/".$i.".png' height='150px'>";
                              echo "   <div class='card-body'>";
                              echo "       <h5 class='card-title'>".$theme_name."</h5>";
                              echo "   </div>";
                              echo "   <ul class='list-group list-group-flush'>";
                              //echo "        <li class='list-group-item'>Quantity: ".$quantity."</li>";
                              echo "        <a role='button' class='btn btn-primary' href='set_completion_see.php?theme_id=".$theme_id."&theme_name=".$theme_name."'>See more</a>";
                              echo "   </ul>";
                              echo "</div>";
                          }
                      ?>
                  </div>
              </div>
          </div>
      </div>
    </body>
  </html>
