<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset ="utf-8"/>
        <title>Search Set</title>
        <!--CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="design/main.css">

        <!--JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>

    <?php
    include 'nav.php';
    include 'bdd/connect.php';
    ?>

    <div class="outer-scontainer">
        <?php
            if (isset($_POST["setnumber"])) {
                include 'set_select.php';
            }
        ?>
    </div>

    <div class="container-fluid">
        <div class="row" id="RowStyle">
            <div class="col-xs-4 offset-xs-4 col-sm-4 offset-sm-4">
                <h1>Chercher un set</h1><br>
                <form action="" method="post" name="ajoutset" id="ajoutset" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="setnumber">Numéro du set</label>
                        <input type="text" class="form-control" name="setnumber" id="setnumber" placeholder="ID">
                    </div>
                    <button type="submit" class="btn btn-primary">Chercher</button>
                </form>
            </div>
        </div>
    </div>
</html>
