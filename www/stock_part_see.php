<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset ="utf-8"/>
        <link rel="stylesheet" type="text/css" href="style/form.css">
        <title>See stock parts</title>
        <!--CSS-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="design/main.css">

        <!--JS-->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <style>
          #input_addPiece{
            width: 5em;
          }
        </style>
    </head>

    <script type='text/javascript'>
        function PopupImage(img) {
            w = open("", 'image', 'weigth=toolbar=no,scrollbars=no,resizable=yes, width=510, height=210');
            w.document.write("<html>");
            w.document.write("<script type='text/javascript'>function checksize() { window.resizeTo(document.images[0].width+10,document.images[0].height+35);window.focus(); } <\/script>");
            w.document.write("<body onload='checksize()' onblur='window.close()' onclick='window.close()' topmargin=0 leftmargin=0 marginwidth=0 marginheight=0>");
            w.document.write("<img src='" + img + "' border='0' alt='image' />");
            w.document.write("</body></html>");
            w.document.close();
        }

    </script>

    <?php
        include 'nav.php';
        include 'bdd/connect.php';
        $cat_id = $_GET['cat_id'];
        $cat_name = $_GET['cat_name'];
        if(isset($cat_id)){
            $query = "SELECT stock.part_num, stock.quantity as qtty, bricklink_id as color_bl, rgb, color.name as colorname, color.id as color_id
            FROM stock_parts as stock
            join parts on parts.part_num = stock.part_num
            join colors as color on color.id=stock.color_id
            left join colors_code_id on color.id = colors_code_id.colors_id
            where quantity > 0 and parts.part_cat_id = $cat_id
            order by part_num;";
        }else{
            $query = "SELECT stock.part_num, stock.quantity as qtty, bricklink_id as color_bl, rgb, color.name as colorname, color.id as color_id
            FROM stock_parts as stock
            join colors as color on color.id=stock.color_id
            left join colors_code_id on color.id = colors_code_id.colors_id
            where quantity > 0
            order by part_num;";
        }


        $result = @mysql_query($query);
    ?>

    <body>
        <div class="container-fluid">
            <div class="row" id="RowStyle">
                <div class="col-xs-8 offset-xs-2 col-sm-8 offset-sm-2">
                    <h1>
                        <?php echo $cat_name;?>
                    </h1><br>
                    <div class="table-responsive-lg">
                        <table class="table table-hover table-light">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>part_num</th>
                                    <th>color</th>
                                    <th>quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    while ($row = @mysql_fetch_assoc($result)) {
                                        echo '<tr scope="row">';
                                        $part_num = $row['part_num'];
                                        $color_bl = $row['color_bl'];
                                        $color_name = $row['colorname'];
                                        $image = 'https://img.bricklink.com/ItemImage/PN/'.$color_bl.'/'.$part_num.'.png';
                                        $quantity = $row['qtty'];
                                        @$set_num = $row['set_num'];
                                        @$piece_num = $row['part_num'];
                                        //@$addQuantity =
                                        echo '<td><a href="javascript:PopupImage(\''.$image.'\')"><img style="max-width: 80px;" src="'.$image.'"></a></td>';
                                        echo '<td><a href="https://www.bricklink.com/v2/catalog/catalogitem.page?P='.$part_num.'&colorName='.$color_bl.'">'.$part_num.'</a><input type="hidden" name="part_num[]" value="'.$part_num.'"></td>';
                                        if ($row['rgb'] == "FFFFFF" || $row['rgb'] == "FCFCFC"){
                                            echo '<td style="border-width:1px; background-color:#'.$row['rgb'].'"><input type="hidden" name="color_id[]" value="'.$row['color_id'].'"><span class=" text-dark" style="background-color:#'.$row['rgb'].';" >'.$row['colorname'].'</span></td>';
                                        }else{
                                            echo '<td style="border-width:1px; background-color:#'.$row['rgb'].'"><input type="hidden" name="color_id[]" value="'.$row['color_id'].'"><span class=" text-light" style="background-color:#'.$row['rgb'].';" >'.$row['colorname'].'</span></td>';
                                        }
                                        echo "<td>".$quantity."</td>";
                                        echo '<td><form class="form-horizontal" action="stock_parts_update.php?cat_id='.$cat_id.'&cat_name='.$cat_name.'&piece='.@$piece_num.'&colorName='.$color_name.'" method="post" name="alterPiece" id="alterPiece" enctype="multipart/form-data">'; //&addQuantity='.$addQuantity.'&removeQuantity='.$removeQuantity.'                  stock_part_see.php?cat_id='.$cat_id.'&piece='.@$piece_num.'&colorName='.$color_name.'
                                        echo '<select name="input_removePiece" id="input_removePiece">';
                                        for($i = 0; $i<=$quantity; $i++){
                                            echo '<option value="'.$i.'">'.$i.'</option>"';
                                        }
                                        echo '</select><button type="submit" id="removePiece" name="removePiece" class="btn-submit">Delete</button>';
                                        echo '<input type="number" name="input_addPiece" id="input_addPiece">';
                                        echo '<button type="submit" id="addPiece" name="addPiece" class="btn-submit">Add</button></form></td>';
                                        echo "</tr>\n";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
