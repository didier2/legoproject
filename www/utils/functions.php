<?php

function isPieceHead($haystack)
{
    return (strpos($haystack, '3626a') !== false) || (strpos($haystack, '3626b') !== false) || (strpos($haystack, '3626c') !== false);
}
function swatch() {
    //Return an RGB array
    $r = ceil(rand(0,255));
    $g = ceil(rand(0,255));
    $b = ceil(rand(0,255));
    return array($r,$g,$b);
}

function rgbtohsv($r,$g,$b) {
    //Convert RGB to HSV
    $r /= 255;
    $g /= 255;
    $b /= 255;
    $min = min($r,$g,$b);
    $max = max($r,$g,$b);

    switch($max) {
        case 0:
            $h = $s = $v = 0;
            break;
        case $min:
            $h = $s = 0;
            $v = $max;
            break;
        default:
            $delta = $max - $min;
            if($r == $max) {
                $h = 0 + ($g - $b) / $delta;
            } elseif($g == $max) {
                $h = 2 + ($b - $r) / $delta;
            } else {
                $h = 4 + ($r - $g) / $delta;
            }
            $h *= 60;
            if($h < 0 ) $h += 360;
            $s = $delta / $max;
            $v = $max;
        }
        return array($h,$s,$v);
}

function hsvtorgb($h,$s,$v) {
    //Convert HSV to RGB
    if($s == 0) {
        $r = $g = $b = $v;
    } else {
        $h /= 60.0;
        $s = $s;
        $v = $v;

        $hi = floor($h);
        $f = $h - $hi;
        $p = ($v * (1.0 - $s));
        $q = ($v * (1.0 - ($f * $s)));
        $t = ($v * (1.0 - ((1.0 - $f) * $s)));

        switch($hi) {
            case 0: $r = $v; $g = $t; $b = $p; break;
            case 1: $r = $q; $g = $v; $b = $p; break;
            case 2: $r = $p; $g = $v; $b = $t; break;
            case 3: $r = $p; $g = $q; $b = $v; break;
            case 4: $r = $t; $g = $p; $b = $v; break;
            default: $r = $v; $g = $p; $b = $q; break;
        }
    }
    return array(
        (integer) ($r * 255 + 0.5),
        (integer) ($g * 255 + 0.5),
        (integer) ($b * 255 + 0.5)
    );
}

?>
